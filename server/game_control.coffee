clean_field = [
  [0,0,0,0, 0,0,0,0]
  [0,0,0,0, 0,0,0,0]
  [0,0,0,0, 0,0,0,0]
  [0,0,0,2, 1,0,0,0]

  [0,0,0,1, 2,0,0,0]
  [0,0,0,0, 0,0,0,0]
  [0,0,0,0, 0,0,0,0]
  [0,0,0,0, 0,0,0,0]
]

pp_games = new Meteor.Collection 'pp_games'

rand_int = -> Math.floor Math.random()*1000000

@clean_db = ->
  pp_games.remove({created: {$lt: +new Date()/1000 - 48*3600} })

Meteor.methods
  create_new_pp_game: ->
    do clean_db
    id = rand_int()
    while pp_games.findOne(code: id)
      id = rand_int()
    pp_games.insert
      field: clean_field
      turn: 0
      type: 'pp'
      code: id
      created: +new Date()/1000
    id

  create_new_pc_game: ->
    return {
      field: clean_field
      turn: 1
      type: 'pc'
    }

  connect_to_pp_game: (id)->
    game = pp_games.findOne {code: +id}
    pp_games.update game, {$set: {turn: 1}}
    pp_games.findOne {code: +id}

