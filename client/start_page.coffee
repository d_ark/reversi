pp_games = new Meteor.Collection 'pp_games'

Template.main.started = -> Session.get 'started'
Template.main.waiting = -> Session.get 'waiting'
Template.main.connecting = -> Session.get 'connecting'
Template.waiting.game_id = -> Session.get 'game_id'

Deps.autorun ->
  return unless Session.get 'game_id'
  game = pp_games.findOne {code: +Session.get('game_id')}
  if game? and game.turn isnt 0
    Session.set 'game', game
    Session.set 'started', true


color_t = ['', 'black', 'red']

Template.connecting.error = -> Session.get 'error'

Template.game.game = -> Session.get 'game'
Template.game.my_turn = -> Session.get('game').turn is Session.get 'color'
Template.game.turn = ->
  if Session.get('game').turn is Session.get 'color'
    return "Now it's your turn (#{color_t[Session.get 'color']})"
  else
    return "Now it's opponent's turn (#{color_t[3 - Session.get('color')]})"
Template.game.finished = -> Session.get('game').winner?
Template.game.result = ->
  w = +Session.get('game').winner
  return "You win! (you've played as #{color_t[Session.get 'color']})" if w is Session.get('color')
  return "You lose... (you've played as #{color_t[Session.get 'color']})" if w is 3 - Session.get('color')
  return "Draw... (you've played as #{color_t[Session.get 'color']})" if w is 0

Template.main.events
  'click #back': ->
    Session.set 'started', false
    Session.set 'waiting', false
    Session.set 'connecting', false

@start_pc_game = ->
  Meteor.call 'create_new_pc_game', (e, r)->
    Session.set 'game', r
    Session.set 'started', true
    Session.set 'color', 1

Template.start_game.events
  'click #start_pc1': ->
    do start_pc_game
    Session.set 'level', 1

  'click #start_pc2': ->
    do start_pc_game
    Session.set 'level', 2

  'click #create_pp': ->
    Meteor.call 'create_new_pp_game', (e, r)->
      Session.set 'waiting', true
      Session.set 'game_id', r
      Session.set 'color', 1

  'click #connect_to_pp': ->
    Session.set 'error', ''
    Session.set 'connecting', true
    Session.set 'color', 2


Template.connecting.events
  'click #connecting_ready': ->
    id = $('#game_id').val()
    Meteor.call 'connect_to_pp_game', id, (e, r)->
      if r?
        Session.set 'game_id', id
        Session.set 'started', true
      else
        Session.set 'error', 'not found...'

@Reversi_helper =
  ways: (get, x, y, my_color, other_color)->
    [ [ 1,  0], [ 1,  1], [ 0,  1], [-1,  1],
      [-1,  0], [-1, -1], [ 0, -1], [ 1, -1] ].filter (way)->
      dx = way[0]
      dy = way[1]
      nx = x + dx
      ny = y + dy
      possible = false
      if get(x + dx, y + dy) is other_color
        while get(nx, ny) is other_color
          nx = nx + dx
          ny = ny + dy
        if get(nx, ny) is my_color
          possible = true
      possible
  any_ways: (get, my, other)->
    for x in [0..7]
      for y in [0..7]
        if get(x,y) is 0
          ways = Reversi_helper.ways get, x, y, my, other
          return true if ways.length
    return false

  check_for_winer: (game)->
    count = [0, 0, 0]
    for row in game.field
      for cell in row
        count[cell] = count[cell] + 1
    if count[1] > count[2]
      game['winner'] = '1'
    else
      if count[1] < count[2]
        game['winner'] = '2'
      else
        game['winner'] = '0'

  go: (game, get, ways, x, y, my, other) ->
    for way in ways
      dx = way[0]
      dy = way[1]
      nx = x + dx
      ny = y + dy
      while get(nx, ny) is other
        game.field[nx][ny] = my
        nx = nx + dx
        ny = ny + dy
    game.field[x][y] = my


  try_to_go: (game, x, y) ->
    possible = false
    my_color = game.turn
    other_color = 3 - my_color

    get = (x,y)->
      return game.field[x][y] if x >= 0 and y >= 0 and x < 8 and y < 8
      0

    return unless get(x,y) is 0

    ways = Reversi_helper.ways get, x, y, my_color, other_color

    return unless ways.length

    Reversi_helper.go game, get, ways, x, y, my_color, other_color

    if Reversi_helper.any_ways get, other_color, my_color
      game.turn = other_color
    else
      unless Reversi_helper.any_ways get, my_color, other_color
        Reversi_helper.check_for_winer game

    game

  possible_gos: (game, count_plus)->
    res = []
    for x in [0..7]
      for y in [0..7]
        g = Reversi_helper.try_to_go game(), x, y
        if g?
          go = {x: x, y: y, game: g}
          if count_plus
            og = game()
            my = og.turn
            plus = 0
            for i in [0..7]
              for j in [0..7]
                if og.field[i][j] isnt g.field[i][j]
                  if g.field[i][j] is my
                    plus += 1
                  else
                    plus -= 1
            go.plus = plus
          res.push go
    return res


  pc_go_1: (game) ->
    gos = Reversi_helper.possible_gos game
    for go in gos
      if go.game.winner is game().turn
        return go.game
    return gos[Math.floor(Math.random()*gos.length)].game

  pc_go_2: (game) ->
    gos = Reversi_helper.possible_gos game, true
    opt = gos[0]
    for go in gos
      if go.game.winner is game().turn
        return go.game
      opt = go if go.plus > opt.plus
    return opt.game


  pc_go: (game, level) ->
    return Reversi_helper.pc_go_1(game) if +level is 1
    return Reversi_helper.pc_go_2(game) if +level is 2



@let_pc_go = (game)->
  return unless game().type is 'pc'
  return if game().turn is Session.get 'color'
  return if game().winner?
  g = Reversi_helper.pc_go(game, Session.get('level'))
  setTimeout ->
    Session.set 'game', g
    if (game().turn isnt Session.get 'color') and (not game().winner?)
      @let_pc_go game
  , 500

Template.game.events
  'click .game-cell': (ev)->
    target = $(ev.target)
    target = target.parent() unless target.is '.game-cell'
    x = target.parent().index()
    y = target.index()
    if Session.get('game').turn is Session.get 'color'
      g = Reversi_helper.try_to_go Session.get('game'), x, y
      if g?
        if g.type is 'pp'
          pp_games.update {_id: g._id}, g
        else
          Session.set 'game', g
          let_pc_go -> Session.get 'game'

